import { IoChatbubbles } from 'react-icons/io5';
import Button from '@/components/atoms/Button';
import RightProfile from '@/components/organism/RightProfile';
import DataForm from '@/components/organism/DataForm';
import Layout from '@/components/templates/Layout';

const App = () => {
	return (
		<Layout>
			<section className="md:flex md:space-x-8">
				<RightProfile />

				<DataForm />
			</section>

			<Button className="fixed bottom-5 right-6 rounded-md bg-primary-500 px-4 py-3 transition duration-300 hover:bg-primary-600">
				<IoChatbubbles size={30} className="text-white" />
				<span className="sr-only">Chat Button</span>
			</Button>
		</Layout>
	);
};

export default App;
