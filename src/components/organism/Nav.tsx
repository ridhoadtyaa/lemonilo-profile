import { FiChevronDown } from 'react-icons/fi';
import Logo from '@/assets/logo.png';
import { IoMdCart, IoMdNotifications } from 'react-icons/io';
import Button from '@/components/atoms/Button';
import photoProfile from '@/assets/photo-profile.png';
import { BsCoin } from 'react-icons/bs';
import Searchbar from '@/components/mollecules/Searchbar';
import { HiOutlineMenuAlt3 } from 'react-icons/hi';

const Navbar: React.FunctionComponent = () => {
	return (
		<nav className="bg-white py-2 shadow-md">
			<div className="layout flex items-center justify-between">
				<img src={Logo} alt="logo" width={180} />
				<ul className="hidden items-center space-x-4 font-semibold sm:flex">
					<li>
						<a href="/kategori">Kategori</a>
					</li>
					<li>
						<a href="/promo">Promo</a>
					</li>
					<li>
						<a href="/life">Life</a>
					</li>
				</ul>
				<Searchbar />
				<div className="hidden items-center space-x-3 lg:flex">
					<Button>
						<IoMdCart size={40} className="text-primary-500" />
						<span className="sr-only">Cart Button</span>
					</Button>
					<Button>
						<IoMdNotifications size={40} className="text-slate-300" />
						<span className="sr-only">Notification Button</span>
					</Button>
				</div>
				<a href="/profile" className="hidden items-center justify-between space-x-7 rounded-full p-2 shadow lg:flex">
					<div className="flex items-center space-x-3">
						<img src={photoProfile} width="40" alt="Profile" />
						<div>
							<p className="text-sm font-semibold text-primary-500">Ridho Aditya</p>
							<div className="flex items-center space-x-1">
								<BsCoin className="text-yellow-500" />
								<span className="text-sm">0</span>
							</div>
						</div>
					</div>
					<FiChevronDown size={18} />
				</a>
				<Button className="block lg:hidden">
					<HiOutlineMenuAlt3 size={30} className="text-primary-700" />
				</Button>
			</div>
		</nav>
	);
};

export default Navbar;
