import Input from '@/components/atoms/Input';
import Button from '@/components/atoms/Button';
import { AiOutlineCheck } from 'react-icons/ai';
import ButtonPrimary from '@/components/mollecules/ButtonPrimary';

const DataForm: React.FunctionComponent = () => {
	return (
		<div className="mt-10 w-full bg-white drop-shadow-1 md:mt-0 md:w-9/12">
			<div className="border-b border-slate-300 px-10">
				<Button className="w-fit border-b-[3px] border-primary-500 px-7 py-4 text-lg font-medium text-primary-500">Data Diri</Button>
			</div>

			<form className="px-10 py-10">
				<div className="flex items-center space-x-4">
					<div className="w-full">
						<label htmlFor="firstName" className="block">
							Nama Depan
						</label>
						<Input
							className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
							defaultValue="Ridho"
							id="firstName"
						/>
					</div>
					<div className="w-full">
						<label htmlFor="lastName" className="block">
							Nama Belakang
						</label>
						<Input
							className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
							defaultValue="Aditya"
							id="lastName"
						/>
					</div>
				</div>

				<div className="mt-6 w-full">
					<div className="flex items-center space-x-4">
						<label htmlFor="email">Email</label>
						<div className="flex w-fit items-center space-x-2 rounded-md bg-primary-500 py-1 px-2 text-xs text-white">
							<AiOutlineCheck /> <span>Terverifikasi</span>
						</div>
					</div>
					<div className="relative">
						<Input
							className="mt-2 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
							defaultValue="akuganteng@gmail.com"
							type="email"
							id="email"
						/>
						<Button className="absolute top-4 right-4 text-sm text-primary-500" type="button">
							Ubah
						</Button>
					</div>
				</div>

				<div className="mt-12 flex items-center space-x-4">
					<div className="w-full">
						<label htmlFor="jenis" className="block">
							Jenis Kelamin
						</label>
						<select
							name="jenis"
							id="jenis"
							className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
						>
							<option value="l">Laki - Laki</option>
							<option value="p">Perempuan</option>
						</select>
					</div>
					<div className="w-full">
						<label htmlFor="dateBorn" className="block">
							Tanggal Lahir
						</label>
						<Input
							className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
							id="dateBorn"
							type="date"
						/>
					</div>
				</div>

				<div className="mt-6 w-full md:w-6/12">
					<label htmlFor="number_phone" className="block">
						Nomor Ponsel
					</label>
					<Input
						className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
						defaultValue={62}
						type="number"
						id="number_phone"
					/>
				</div>

				<div className="mt-6 w-full">
					<label htmlFor="password" className="block">
						Password
					</label>
					<div className="relative">
						<Input
							className="mt-1 w-full rounded-md border-slate-300 text-sm text-slate-500 focus:border-none focus:ring-primary-400"
							type="password"
							id="password"
							placeholder="Buat Password"
						/>
						<Button className="absolute top-3 right-4 text-sm text-primary-500" type="button">
							Ubah
						</Button>
					</div>
				</div>

				<ButtonPrimary className="mx-auto mt-10 flex w-7/12 py-3" type="submit">
					Simpan
				</ButtonPrimary>
			</form>
		</div>
	);
};

export default DataForm;
