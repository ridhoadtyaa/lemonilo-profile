import Button from '@/components/atoms/Button';
import photoProfile from '@/assets/photo-profile.png';
import { BsFillPencilFill, BsCoin, BsChevronRight, BsCreditCard } from 'react-icons/bs';
import { FaLemon } from 'react-icons/fa';
import { IoIdCardOutline, IoLocationOutline } from 'react-icons/io5';
import { RiNewspaperFill, RiFileEditLine, RiLogoutBoxLine } from 'react-icons/ri';

const RightProfile = () => {
	return (
		<div className="w-full md:w-3/12">
			<div className="flex flex-col items-center rounded-md bg-primary-200/80 p-6">
				<div className="relative overflow-hidden rounded-full">
					<img src={photoProfile} width="120" alt="Profile" />
					<div className="absolute bottom-1 flex w-full items-center justify-center bg-slate-400 py-1 text-white opacity-50">
						<BsFillPencilFill />
						<span>Ubah</span>
					</div>
				</div>
				<h5 className="mt-4 font-semibold">Ridho Aditya</h5>
				<p className="mt-1 text-sm text-slate-400/80">Moni Coins</p>
				<Button className="mt-2 w-7/12 justify-between rounded-full bg-white py-1 px-2">
					<div className="flex items-center space-x-2">
						<BsCoin className="text-yellow-500" size="20" />
						<span>0</span>
					</div>
					<div className="rounded-full bg-primary-500 p-1 text-white">
						<BsChevronRight />
					</div>
				</Button>
			</div>

			<div className="mt-4">
				<div className="mx-auto  w-11/12 overflow-hidden rounded-full bg-slate-200/80">
					<div className="w-9/12 rounded-full bg-gradient-to-r from-primary-100 to-primary-500 py-2" />
				</div>
				<p className="mt-2 text-center">Profil Anda 67% Lengkap!</p>
			</div>

			<Button className="mt-4 w-full justify-between rounded-md border border-primary-500 bg-primary-200/50 py-3 px-4 transition duration-300 hover:bg-primary-200">
				<div>
					<p className="text-sm text-slate-400">Lemon Points</p>
					<div className="mt-1 flex items-center space-x-2">
						<FaLemon className="text-yellow-400" />
						<span>0</span>
					</div>
				</div>
				<BsChevronRight className="text-primary-500" size={20} />
			</Button>

			<div className="mt-6 flex w-full flex-col divide-y-[1px] bg-white px-6 drop-shadow-1">
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<IoIdCardOutline className="text-primary-400" size="20" />
					<span>Tentang Saya</span>
				</Button>
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<RiNewspaperFill className="text-primary-400" size="20" />
					<span>Pesanan Saya</span>
				</Button>
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<RiFileEditLine className="text-primary-400" size="20" />
					<span>Ulasan Saya</span>
				</Button>
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<IoLocationOutline className="text-primary-400" size="20" />
					<span>Pengaturan Alamat</span>
				</Button>
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<BsCreditCard className="text-primary-400" size="20" />
					<span>Kartu Saya</span>
				</Button>
				<Button className="justify-start space-x-4 py-2 hover:text-primary-500">
					<RiLogoutBoxLine className="text-primary-400" size="20" />
					<span>Keluar</span>
				</Button>
			</div>
		</div>
	);
};

export default RightProfile;
