import Ads from '@/components/mollecules/Ads';
import Navbar from './Nav';

const Header: React.FunctionComponent = () => {
	return (
		<header className="fixed top-0 z-50 w-full">
			<Ads />
			<Navbar />
		</header>
	);
};

export default Header;
