type InputProps = React.DetailedHTMLProps<React.InputHTMLAttributes<HTMLInputElement>, HTMLInputElement>;

const Input: React.FunctionComponent<InputProps> = ({ type = 'text', className, ...props }) => {
	return <input type={type} className={className} {...props} />;
};

export default Input;
