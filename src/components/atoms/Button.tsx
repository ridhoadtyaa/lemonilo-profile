import { createElement } from 'react';
import { twMerge } from 'tailwind-merge';

type ButtonProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

const Button: React.FunctionComponent<ButtonProps> = ({ className, ...props }) => {
	return createElement('button', {
		...props,
		className: twMerge('inline-flex items-center justify-center outline-none', className),
	});
};

export default Button;
