import { createElement } from 'react';
import { twMerge } from 'tailwind-merge';

type ButtonProps = React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>;

const ButtonPrimary: React.FunctionComponent<ButtonProps> = ({ className, ...props }) => {
	return createElement('button', {
		...props,
		className: twMerge(
			'rounded-md bg-primary-500 hover:opacity-90 transition duration-300 py-2 px-4 font-medium text-white inline-flex items-center justify-center',
			className
		),
	});
};

export default ButtonPrimary;
