import { BiSearchAlt } from 'react-icons/bi';
import Input from '@/components/atoms/Input';

const Searchbar: React.FunctionComponent = () => {
	return (
		<div className="relative hidden w-full max-w-sm lg:block 2xl:max-w-3xl">
			<Input
				className="block w-full rounded-md border-0 bg-gray-200/60 px-12 placeholder:text-sm placeholder:font-medium placeholder:text-slate-400 focus:ring-primary-300"
				placeholder="Hai Ridho, Cari produk apa hari ini?"
			/>
			<BiSearchAlt className="absolute top-2 left-3 rotate-90 text-slate-400" size={23} />
		</div>
	);
};

export default Searchbar;
