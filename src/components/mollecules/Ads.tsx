import { FiPhone } from 'react-icons/fi';
import { GiSmartphone } from 'react-icons/gi';

const Ads: React.FunctionComponent = () => {
	return (
		<section className="hidden bg-primary-100 md:block">
			<div className="layout flex justify-between py-2">
				<div className="flex items-start space-x-1 text-xs font-medium text-primary-700">
					<GiSmartphone size="20" /> <span>Download Aplikasi Lemonilo</span>
				</div>
				<div className="flex items-start space-x-1 text-xs font-medium text-primary-700">
					<FiPhone size="16" /> <span>Butuh Bantuan? (+62) 21 4020 0788</span>
				</div>
				<div className="text-xs font-medium text-primary-700">
					Diskon sebesar IDR 500000 dengen kupon <span>BELANJASEHAT</span> untuk pelanggan baru
				</div>
				<div className="text-xs font-medium text-primary-700">Kenapa Belanja di Lemonilo?</div>
			</div>
		</section>
	);
};

export default Ads;
