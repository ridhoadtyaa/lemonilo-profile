import Header from '../organism/Header';

interface LayoutProps {
	children: React.ReactNode;
}

const Layout: React.FunctionComponent<LayoutProps> = ({ children }) => {
	return (
		<>
			<Header />
			<main className="layout mt-48 pb-20 2xl:w-8/12">{children}</main>
		</>
	);
};

export default Layout;
