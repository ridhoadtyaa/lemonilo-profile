/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');
const { fontFamily } = require('tailwindcss/defaultTheme');

module.exports = {
	content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
	theme: {
		extend: {
			colors: {
				primary: colors.lime,
			},
			fontFamily: {
				primary: ["'Poppins'", ...fontFamily.sans],
			},
			dropShadow: {
				1: '0px 2px 6px rgb(0, 0, 0, 0.2)',
			},
		},
	},
	plugins: [require('@tailwindcss/forms')],
};
